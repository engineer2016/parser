from multiprocessing import Pool
import sys
import csv
import random
import time
from functools import reduce

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import requests
import lxml.html as lh
import pymysql as msq

# для работы с базой MySQL
USER = 'user'
PASSWD = 'qqqqqq'
DB = 'parsing'
#URL_FILE = sys.argv[1]
#PROXY_FILE = sys.argv[2]
# список таблиц в базе mysql
FULLURL = 'fullurl'
MAINURL = 'mainurls'
PROXYLIST = 'proxylist'
PROXY_LIST = 'proxy_list'
# Id для прокси.
COLM_ID = 'id'
# ip прокси.
COLM_PROXY_IP = 'proxy_ip'
# Порт прокси.
COLM_PROXY_PORT = 'proxy_port'
# Доступ к приватным прокси(логин:пароль)
COLM_PROXY_ACCESS = 'proxy_access'
# Время загрузки страници при тестировании.
COLM_SPEED = 'speed'
# Дата проверки прокси
COLM_DATE_ACTUAL = 'date_actual'
# Время начала использования прокси.
COLM_TIME_USING = 'time_using'
# Версия браузера.
COLM_USER_AGENT = 'user_agent'
# Количество использований за последнюю сессию.
COLM_USES_COUNT = 'uses_count'
# Флаг работы: 1 - сейчас работет, 0 - нет.
COLM_STATE = 'state'
# Флаг пригодности прокси: 1 - рабочий, 0 - нет.
COLM_ACTIVE = 'active'
# Установка ошибки прокси
COLM_ERROR_CODE = 'error_code'
# Описание ошибки.
COLM_ERROR_MESSAGE = 'error_message'

FREE_PROXYLIST = 'freeproxy'
WARES = 'wares'
COLM_FULLULR = 'furl'
COLM_MAINURL = 'url'
COLM_WARES_NAME = 'name'
COLM_WARES_MARKET = 'market'
COLM_WARES_PRICE = 'price'
# Время ответа прокси
PROXY_RESPONCE = 0.5

# количество процессов и потоков
AMOUNT_PROCESS = 10
AMOUNT_THREADS = 6
# для работы основного модуля
EK_BASE_URL = 'http://ek.ua'
HOTLINE_BASE_URL = 'http://hotline.ua'
USER_AGENT = [
'Opera/9.80 (Windows NT 6.0) Presto/2.12.388 Version/12.14',
'Opera/9.80 (X11; Linux x86_64; U; fr) Presto/2.9.168 Version/11.50',
'Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25',
'Opera/12.02 (Android 4.1; Linux; Opera Mobi/ADR-1111101157; U; en-US) Presto/2.9.201 Version/12.02',
'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20130406 Firefox/23.0',
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0',
'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
'Mozilla/5.0 (Macintosh; Intel Mac OS X) AppleWebKit/534.34 (KHTML, like Gecko) PhantomJS/1.9.8 Safari/534.34']

COOKIE_KIEV = requests.cookies.RequestsCookieJar()
COOKIE_KIEV.set('PHPSESSID', 'qm7lfihcsbog9ij0mtd7ip1kf0')
COOKIE_KIEV.set('n_session_id_cookie', 'eb6ad6d63f7650c06ba6f5c3f531ad20')
COOKIE_KHAR = requests.cookies.RequestsCookieJar()
COOKIE_KHAR.set('PHPSESSID', 'p99c664br8k7q3f09aspfu63p4')
COOKIE_KHAR.set('n_session_id_cookie', 'eb6ad6d63f7650c06ba6f5c3f531ad20')

# Куки для Киева для хотлайна.
d = {
     'city_id': '187',
     '_ga': 'GA1.2.254460989.1473421715',
     '__utmt': '1', '__utmt_UA-2141710-2': '1',
     '_dc_gtm_UA-2141710-13': '1',
     'PHPSESSID': 'bfd209d7b9daf92b6ad6d0875cd142f1',
     '__utmv': '32703777.|1=RegisteredUser=FALSE=1',
     '__utmc': '32703777', '_gat_UA-2141710-13': '1',
     'gd_order_primary': '0',
     'recent_visited_': 'a:2:{i:0;i:1504727;i:1;i:3781874;}',
     'region_mode': '1',
     'gd_cmp': '0',
     '__utmb': '32703777.2.10.1474034419',
     'region_popup': '3',
     '__utma': '32703777.254460989.1473421715.1474034419.1474034419.1',
     'currency': 'uah',
     'hl_sid': 'c8ec2d7b26ae62da113d5c5ab6ca800d',
     'hluniqueid': 'a630b96956a8f82cb123c14ca3b7551d',
     'region': '1',
     '__utmz': '32703777.1474034419.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
     'hluniqueid_ctl': '6bdb13d2cb6730e9db99e027033b3e5e'
     }

COOKIE_KIEV_HOT = requests.cookies.RequestsCookieJar()
for i in d:
    COOKIE_KIEV_HOT.set(i, d[i])
